﻿using Mono.Cecil;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace BatchProcessAutomater
{
    /// <summary>
    /// Make all interfaces public here on principle (we're running this to make Paint.Net interfaces public...), even though only the first should be...
    /// </summary>
    public class DllTweaker
    {
        public static void ConvertPaintNetAssemblies()
        {
            string UpgradeMarker = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Upgrade.1.txt");
            if (File.Exists(UpgradeMarker))
                return;

            DoConvertPaintNetAssemblies();
            
            File.WriteAllText(UpgradeMarker, "Paint.Net assemblies upgraded to have all-public interfaces!");
        }

        public static void DoConvertPaintNetAssemblies()
        {
            DirectoryInfo di = new DirectoryInfo(AppDomain.CurrentDomain.BaseDirectory);
            var dlls = di.GetFiles("PaintDotNet*.dll", SearchOption.TopDirectoryOnly).AsEnumerable();
            var exes = di.GetFiles("PaintDotNet*.exe", SearchOption.TopDirectoryOnly).AsEnumerable();
            var scriptlab = di.GetFiles("ScriptLab.dll", SearchOption.AllDirectories).AsEnumerable();
            var fis = dlls.Union(exes).Union(scriptlab);

            foreach (var item in fis)
            {
                try
                {
                    ConvertAssemblyToPublic(item.FullName);
                }
                catch (Exception ex)
                {
                    if (ex.Message != "Cannot upgrade mixed mode assembly." &&
                        ex.Message != "Format of the executable (.exe) or library (.dll) is invalid.")
                        throw ex;
                }
            }
        }

        static void ConvertAssemblyToPublic(string AssemblyPath)
        {
            AssemblyDefinition a = AssemblyDefinition.ReadAssembly(AssemblyPath);

            bool isILOnly = (a.MainModule.Attributes & ModuleAttributes.ILOnly) == ModuleAttributes.ILOnly;
            if (!isILOnly)
                throw new Exception("Cannot upgrade mixed mode assembly.");

            EnumTypes(a.MainModule.Types);

            a.Write(AssemblyPath);
        }


        static void EnumTypes(Mono.Collections.Generic.Collection<TypeDefinition> types)
        {
            foreach (TypeDefinition type in types)
            {
                ConvertTypeToPublic(type);
                if (type.NestedTypes.Count > 0)
                    EnumTypes(type.NestedTypes);
            }
        }

        static void ConvertTypeToPublic(TypeDefinition type)
        {
            if (!type.IsPublic)
            {
                if (type.IsNested)
                    type.IsNestedPublic = true;
                else
                    type.IsPublic = true;
            }

            foreach (MethodDefinition method in type.Methods.Where(m => !m.IsConstructor))
                if (!method.IsPublic)
                {
                    if (OverridesSystemMethod(method))
                        continue;
                    method.IsPublic = true;
                }

            foreach (FieldDefinition field in type.Fields)
                if (!field.IsPublic)
                    field.IsPublic = true;
        }

        private static bool OverridesSystemMethod(MethodDefinition method)
        {
            if (method.HasOverrides)
                return SimpleOverridesSystemMethod(method);

            if (method.IsHideBySig && method.IsReuseSlot)
                return ComplexOverridesSystemMethod(method);

            return false;
        }

        private static bool SimpleOverridesSystemMethod(MethodDefinition method)
        {
            foreach (var item in method.Overrides)
            {
                if (item.DeclaringType.FullName.StartsWith("System", StringComparison.OrdinalIgnoreCase))
                    return true;

                MethodDefinition def = item.Resolve();

                if (def.HasOverrides && OverridesSystemMethod(def))
                    return true;
            }

            return false;
        }

        private static bool ComplexOverridesSystemMethod(MethodDefinition method)
        {
            TypeDefinition type = method.DeclaringType;
            TypeDefinition earliestType = null;
            while (true)
            {
                if (type.Methods.Any(m => m.Name == method.Name))
                    earliestType = type;

                if (type.BaseType == null)
                    break;

                type = type.BaseType.Resolve();
            }

            if (earliestType.Namespace.StartsWith("System", StringComparison.OrdinalIgnoreCase))
                return true;

            return false;
        }
    }
}

﻿using PaintDotNet.Actions;
using PaintDotNet.Controls;
using PaintDotNet.Dialogs;
using PaintDotNet.Effects;
using pyrochild.effects.common;
using pyrochild.effects.scriptlab;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace BatchProcessAutomater
{
    class Automation
    {
        static Automation()
        {
            pdnAssembly = typeof(PaintDotNet.SelectionCombineModeExtensions).Assembly;
        }

        public static Assembly pdnAssembly;

        public static void Bridge(string input, string output, string fileFilter, string imageEffects)
        {
            RunProcess(input, output, fileFilter, imageEffects);
        }

        public static void RunProcess(string input, string output, string fileFilter, string imageEffects)
        {
            MainForm MainForm = new MainForm();
            MainForm.Show();
            BulkProcess(MainForm, input, output, fileFilter, imageEffects);
        }

        private static void BulkProcess(MainForm mainForm, string input, string output, string fileFilter, string ScriptPath)
        {
            try
            {
                //TODO: This function and publicise scriptlab plugin
                DirectoryInfo di = new DirectoryInfo(input);
                FileInfo[] fis = di.GetFiles(fileFilter);

                ConfigDialog d = new ConfigDialog();
                d.LoadFile(ScriptPath);
                
                ConfigToken token = new ConfigToken();
                token.script = (ScriptLabScript)d.sls;
                token.effects = d.AvailableEffects;
                ScriptEffectProxy.OverrideEffectToken = token;

                for (int i = 0; i < fis.Length; i++)
                {
                    mainForm.appWorkspace.OpenFileInNewWorkspace(fis[i].FullName); //Open file
                    mainForm.appWorkspace.toolBar.mainMenu.effectsMenu.RunEffect(typeof(ScriptEffectProxy));   
                    string SavePath = Path.Combine(
                            output,
                            fis[i].Name
                        );
                    
                    SaveAs(mainForm.appWorkspace, SavePath);
                    mainForm.appWorkspace.PerformAction(new CloseWorkspaceAction(mainForm.appWorkspace.activeDocumentWorkspace)); //Close file
                }
            }
            catch (Exception ex)
            {
            }
        }

        private static void SaveAs(AppWorkspace appWorkspace, string FileName)
        {
            DocumentWorkspace activeDocumentWorkspace = appWorkspace.activeDocumentWorkspace;
            activeDocumentWorkspace.filePath = FileName;
            activeDocumentWorkspace.fileType = new NoDialogJPEG();
            activeDocumentWorkspace.DoSave(false);
        }

        
    }
}
